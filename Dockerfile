FROM php:5.6-apache

RUN echo "date.timezone=Europe/Paris" > /usr/local/etc/php/php.ini

RUN apt-get update && apt-get install -y git zlib1g-dev wkhtmltopdf libicu-dev

RUN docker-php-ext-install -j$(nproc) pdo pdo_mysql zip intl

RUN a2enmod rewrite
